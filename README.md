# GB Studio font tool

> Compare two GB Studio fonts.

## Usage

Go to [edo999.itch.io/gb-studio-font-tool](https://edo999.itch.io/gb-studio-font-tool) and click on `Run tool`.

Drag and drop two GB Studio font PNG files onto the page. An image showing each glyph aligned is displayed so you can visually compare the glyphs from both fonts.

Use the magnifier tool to zoom in and out.

## Contribute

Clone this git repo and install the dependencies:

```bash
yarn
```

Start the development server:

```bash
yarn start
```

Point your web browser to the URL indicated.

Any changes made to the code are immediately reflected.

## Build

If you want to host it yourself, you can a production build with this command:

```bash
yarn build
```

The content of the `dist` folder can now be published on a website.
