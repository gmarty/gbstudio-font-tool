const Zoom = ({ hidden, increment, decrement }) => {
  return (
    <div
      className={`${
        hidden ? 'hidden' : ''
      } fixed bottom-0 right-0 p-6 text-slate-100 dark:text-slate-800 flex flex-col`}
    >
      <button
        onClick={increment}
        className="w-10 h-10 bg-slate-500 dark:bg-slate-300 hover:bg-slate-600 dark:hover:bg-slate-400 hover:text-slate-200 dark:hover:text-slate-900 rounded-t-full mb-1 ease-in-out duration-200"
      >
        <MagnifyingGlassPlusIcon />
      </button>
      <button
        onClick={decrement}
        className="w-10 h-10 bg-slate-500 dark:bg-slate-300 hover:bg-slate-600 dark:hover:bg-slate-400 hover:text-slate-200 dark:hover:text-slate-900 rounded-b-full ease-in-out duration-200"
      >
        <MagnifyingGlassMinusIcon />
      </button>
    </div>
  );
};

const MagnifyingGlassPlusIcon = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 24 24"
    fill="currentColor"
    className="w-6 h-6 mx-auto"
  >
    <path
      fillRule="evenodd"
      d="M10.5 3.75a6.75 6.75 0 100 13.5 6.75 6.75 0 000-13.5zM2.25 10.5a8.25 8.25 0 1114.59 5.28l4.69 4.69a.75.75 0 11-1.06 1.06l-4.69-4.69A8.25 8.25 0 012.25 10.5zm8.25-3.75a.75.75 0 01.75.75v2.25h2.25a.75.75 0 010 1.5h-2.25v2.25a.75.75 0 01-1.5 0v-2.25H7.5a.75.75 0 010-1.5h2.25V7.5a.75.75 0 01.75-.75z"
      clipRule="evenodd"
    />
  </svg>
);

const MagnifyingGlassMinusIcon = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 24 24"
    fill="currentColor"
    className="w-6 h-6 mx-auto"
  >
    <path
      fillRule="evenodd"
      d="M10.5 3.75a6.75 6.75 0 100 13.5 6.75 6.75 0 000-13.5zM2.25 10.5a8.25 8.25 0 1114.59 5.28l4.69 4.69a.75.75 0 11-1.06 1.06l-4.69-4.69A8.25 8.25 0 012.25 10.5zm4.5 0a.75.75 0 01.75-.75h6a.75.75 0 010 1.5h-6a.75.75 0 01-.75-.75z"
      clipRule="evenodd"
    />
  </svg>
);

export default Zoom;
