import React, { useState } from 'react';
import Warning from './Warning';
import Table from './Table';
import Zoom from './Zoom';

const App = () => {
  const [highlighted, setHighlighted] = useState(false);
  const [warning, setWarning] = useState('');
  const [fonts, setFonts] = useState([]);
  const [zoom, setZoom] = useState(6);

  const containerStyle = !highlighted
    ? 'text-slate-400 dark:text-slate-300 border-slate-400 dark:border-slate-400'
    : 'text-green-600 dark:text-green-300 border-green-600 dark:border-green-400 bg-green-100 dark:bg-green-800';
  const dragDiv =
    fonts.length === 0 ? (
      <div
        className={`${containerStyle} ease-in-out duration-200 mt-6 p-6 h-60 rounded-lg border-dashed border-2 flex flex-col justify-center content-evenly text-center`}
        onDragEnter={(e) => {
          setHighlighted(true);
          preventDefaults(e);
        }}
        onDragOver={(e) => {
          setHighlighted(true);
          preventDefaults(e);
        }}
        onDragLeave={(e) => {
          setHighlighted(false);
          preventDefaults(e);
        }}
        onDragEnd={(e) => {
          setHighlighted(false);
          preventDefaults(e);
        }}
        onDrop={(e) => {
          preventDefaults(e);

          setHighlighted(false);
          const droppedFiles = [...e.dataTransfer.files];

          setWarning();
          const hasWarning = droppedFiles.some((file) => {
            if (file.type !== 'image/png') {
              setWarning('Only PNG files are supported. Please try again.');
              return true;
            }
          });
          if (hasWarning) {
            return;
          }

          if (droppedFiles.length !== 2) {
            setWarning('Please drag and drop two different files.');
            return;
          }

          droppedFiles.forEach((file, i) => {
            let img = new Image();

            img.onload = () => {
              const { width, height, src } = img;

              if (width !== 128 && height !== 112) {
                setWarning('Font images must be 128×112 pixels. Try again');

                URL.revokeObjectURL(src);
                img = null;

                return;
              }

              fonts.push(img);
              setFonts([...fonts]);
            };

            img.src = URL.createObjectURL(file);
          });
        }}
      >
        <ArrowDownTrayIcon />
        <p className="mt-2 text-sm">
          Please drag and drop two<br></br>
          <b>GB Studio PNG font files</b> here.
        </p>
      </div>
    ) : null;

  const incrementZoom = () => {
    setZoom(Math.min(zoom + 1, 20));
  };
  const decrementZoom = () => {
    setZoom(Math.max(zoom - 1, 1));
  };

  return (
    <>
      <Warning message={warning} />
      {dragDiv}
      <Table fonts={fonts} zoom={zoom} className="mx-auto mt-8" />
      <Zoom
        hidden={fonts.length < 2}
        increment={incrementZoom}
        decrement={decrementZoom}
      />
    </>
  );
};

const preventDefaults = (e) => {
  e.preventDefault();
  e.stopPropagation();
};

const ArrowDownTrayIcon = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 24 24"
    fill="currentColor"
    className="w-12 h-12 mx-auto"
  >
    <path
      fillRule="evenodd"
      d="M12 2.25a.75.75 0 01.75.75v11.69l3.22-3.22a.75.75 0 111.06 1.06l-4.5 4.5a.75.75 0 01-1.06 0l-4.5-4.5a.75.75 0 111.06-1.06l3.22 3.22V3a.75.75 0 01.75-.75zm-9 13.5a.75.75 0 01.75.75v2.25a1.5 1.5 0 001.5 1.5h13.5a1.5 1.5 0 001.5-1.5V16.5a.75.75 0 011.5 0v2.25a3 3 0 01-3 3H5.25a3 3 0 01-3-3V16.5a.75.75 0 01.75-.75z"
      clipRule="evenodd"
    />
  </svg>
);

export default App;
