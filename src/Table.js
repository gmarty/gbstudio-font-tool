import React, { useRef, useEffect } from 'react';

const GLYPH_NUMBER = 224;
const PADDING = 2;

const Table = ({ fonts = [], zoom = 1, className }) => {
  const canvasRef = useRef(null);

  const width = 8 * 2 + PADDING * (fonts.length + 1);
  const height = GLYPH_NUMBER * 8 + PADDING * (GLYPH_NUMBER + 1);

  useEffect(() => {
    let canvas = canvasRef.current;
    const ctx = canvas.getContext('2d');

    ctx.fillStyle = 'hsl(0 0% 50%)';
    ctx.fillRect(0, 0, width, height);

    fonts.forEach((font, i) => {
      draw(ctx, font, i);
    });
  }, [fonts, zoom]);

  return (
    <canvas
      className={`${fonts.length < 2 ? 'hidden ' : ''}${className}`}
      ref={canvasRef}
      width={width}
      height={height}
      style={{ width: width * zoom, height: height * zoom }}
    />
  );
};

const draw = (ctx, font, i = 0) => {
  for (let glyph = 0; glyph < GLYPH_NUMBER; glyph++) {
    // drawImage(image, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight)
    const sx = (glyph % 16) * 8;
    const sy = Math.floor(glyph / 16) * 8;
    const dx = 8 * i + PADDING * (i + 1);
    const dy = PADDING + glyph * (8 + PADDING);
    ctx.drawImage(font, sx, sy, 8, 8, dx, dy, 8, 8);
  }
};

export default Table;
